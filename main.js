const electron = require('electron')
const path = require('path')
const url = require('url')

const { app, BrowserWindow, TouchBar, ipcMain, NativeImage } = require('electron')
const {TouchBarButton, TouchBarLabel, TouchBarSpacer} = TouchBar
const iconPath = __dirname + '/icons/';

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

const playB = new TouchBarButton({
    icon: iconPath + 'play.png',
    backgroundColor: '#007bff',
    click: () => {
        sendCmd('pause');
    }
});
const prevB = new TouchBarButton({
    icon: iconPath + 'previous.png',
    click: () => {
        sendCmd('prev');
    }
    // backgroundColor: '#007bff'
});
const nextB = new TouchBarButton({
    icon: iconPath + 'next.png',
    click: () => {
        sendCmd('next');
    }
});

const tLabel = new TouchBarLabel({})

const touchBar = new TouchBar([
    // new TouchBarSpacer({size: 'flexible'}),
    prevB,
    // new TouchBarSpacer({size: 'small'}),
    playB,
    // new TouchBarSpacer({size: 'small'}),
    nextB,
    new TouchBarSpacer({size: 'small'}),
    tLabel

]);

function createWindow() {
    const WEB_FOLDER = 'gui/dist/lameapp';
    const PROTOCOL = 'file';
  
    electron.protocol.interceptFileProtocol(PROTOCOL, (request, callback) => {
        // // Strip protocol
        let url = request.url.substr(PROTOCOL.length + 1);
  
        // Build complete path for node require function
        url = path.join(__dirname, WEB_FOLDER, url);
  
        // Replace backslashes by forward slashes (windows)
        // url = url.replace(/\\/g, '/');
        url = path.normalize(url);
  
        callback({path: url});
    });
    // Create the browser window.
    win = new BrowserWindow({
        width: 1280,
        height: 720,
        backgroundColor: '#292c32',
        darkTheme: true,
        // titleBarStyle: 'hidden',
        autoHideMenuBar: true,
        // transparent: true
        // webPreferences: {
        //     nodeIntegration: false
        // }
    })

    win.loadURL(url.format({
        pathname: 'index.html',
        protocol: PROTOCOL + ':',
        slashes: true
      }));
    // Open the DevTools.
    // if(process.argv[2] === 'dev') win.webContents.openDevTools()

    if(process.platform === 'darwin') win.setTouchBar(touchBar);
    tLabel.label = 'Track'

    // Emitted when the window is closed.
    win.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null
    })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    // if (process.platform !== 'darwin') {
        app.quit()
    // }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
})

ipcMain.on('track', (event, label) => {
    tLabel.label = label;
    console.log(label);
})

ipcMain.on('status', (event, args) => {
    playB.icon = iconPath + (args.playing ? 'pause.png' : 'play.png');
    console.log(args);
})

function sendCmd(action) {
    win.webContents.send('control', { action });
}